#!/bin/sh
i=0

while true ; 
do
  stackStatus=$(aws cloudformation describe-stacks --stack-name todo-app --region us-east-1 | jq '.Stacks | .[] | .StackStatus' | tr -d '"')
  echo "Criando recursos..."
  [ "$stackStatus" == "CREATE_COMPLETE" ] && echo "Recursos criados" && exit 0
  [ "$stackStatus" == "ROLLBACK_COMPLETE" ] && echo "Erro ao criar recursos" && exit 1
  sleep 20
done