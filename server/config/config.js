const { DB_HOST, NODE_ENV } = process.env;

// if (env === 'development' || env === 'test') {
//   const config = require('./config.json');
//   const envConfig = config[env];
//   Object.keys(envConfig).forEach((key) => {
//     process.env[key] = envConfig[key];
//   });
// }

if (NODE_ENV === 'development') {
  process.env.PORT = 3000;
  process.env.MONGODB_URI = 'mongodb://localhost:27017/TodoAppTest';
  process.env.JWT_SECRET = '123019dnamnlksad19j123capaanlapz12';
} else if (NODE_ENV === 'test') {
  process.env.PORT = 3000;
  process.env.MONGODB_URI = `mongodb://${DB_HOST || 'mongodb'}:27017/TodoAppTest`;
  process.env.JWT_SECRET = 'asdlksajdfas0897123ncsa12adamn12809acsn';
}
